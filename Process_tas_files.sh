#!/bin/bash
#
# This long script will use the NetCDF Operators (NCO) to create some useful smaller files of surface air temperatures.
# Aiming for time-mean averages with areacella attached

cd /data/aod/ElsamBrierley_Processed_Files/

#####CCSM4
#lgm
ncra -O /data/CMIP/pmip3/output/NCAR/CCSM4/lgm/monClim/atmos/Aclim/r1i1p1/v20140428/tas/tas_Aclim_CCSM4_lgm_r1i1p1_180001-190012-clim.nc CCSM4/tas_Aavg_CCSM4_lgm_r1i1p1.nc
ncks -A -v areacella /data/CMIP/cmip5/output1/NCAR/CCSM4/piControl/fx/atmos/fx/r0i0p0/v20120413/areacella/areacella_fx_CCSM4_piControl_r0i0p0.nc CCSM4/tas_Aavg_CCSM4_lgm_r1i1p1.nc
#midHolocene
ncra -O /data/CMIP/pmip3/output/NCAR/CCSM4/midHolocene/monClim/atmos/Aclim/r1i1p1/v20140428/tas/tas_Aclim_CCSM4_midHolocene_r1i1p1_100001-130012-clim.nc CCSM4/tas_Aavg_CCSM4_midHolocene_r1i1p1.nc
ncks -A -v areacella /data/CMIP/cmip5/output1/NCAR/CCSM4/piControl/fx/atmos/fx/r0i0p0/v20120413/areacella/areacella_fx_CCSM4_piControl_r0i0p0.nc CCSM4/tas_Aavg_CCSM4_midHolocene_r1i1p1.nc
#piControl
ncra -O /data/CMIP/pmip3/output/NCAR/CCSM4/piControl/monClim/atmos/Aclim/r1i1p1/v20140428/tas/tas_Aclim_CCSM4_piControl_r1i1p1_025001-130012-clim.nc CCSM4/tas_Aavg_CCSM4_piControl_r1i1p1.nc
ncks -A -v areacella /data/CMIP/cmip5/output1/NCAR/CCSM4/piControl/fx/atmos/fx/r0i0p0/v20120413/areacella/areacella_fx_CCSM4_piControl_r0i0p0.nc CCSM4/tas_Aavg_CCSM4_piControl_r1i1p1.nc
#rcp85
ncra -O -d time,'2081-01-01','2100-12-31' /data/CMIP/cmip5/output1/NCAR/CCSM4/rcp85/mon/atmos/Amon/r1i1p1/v20130426/tas/tas_Amon_CCSM4_rcp85_r1i1p1_200601-210012.nc CCSM4/tas_Aavg_CCSM4_rcp85_r1i1p1_208101-210012.nc
ncks -A -v areacella /data/CMIP/cmip5/output1/NCAR/CCSM4/piControl/fx/atmos/fx/r0i0p0/v20120413/areacella/areacella_fx_CCSM4_piControl_r0i0p0.nc CCSM4/tas_Aavg_CCSM4_rcp85_r1i1p1_208101-210012.nc
#abrupt4xCO2
ncra -O -d time,'1981-01-01','2000-12-31' /data/CMIP/cmip5/output1/NCAR/CCSM4/abrupt4xCO2/mon/atmos/Amon/r1i1p1/v20120604/tas/tas_Amon_CCSM4_abrupt4xCO2_r1i1p1_185001-200012.nc CCSM4/tas_Aavg_CCSM4_abrupt4xCO2_r1i1p1_198101-200012.nc
ncks -A -v areacella /data/CMIP/cmip5/output1/NCAR/CCSM4/piControl/fx/atmos/fx/r0i0p0/v20120413/areacella/areacella_fx_CCSM4_piControl_r0i0p0.nc CCSM4/tas_Aavg_CCSM4_abrupt4xCO2_r1i1p1_198101-200012.nc

#####CNRM-CERFACS
area_file="/data/CMIP/cmip5/output1/CNRM-CERFACS/CNRM-CM5/piControl/fx/atmos/fx/r0i0p0/v20130826/areacella/areacella_fx_CNRM-CM5_piControl_r0i0p0.nc"
lgm_file="/data/CMIP/pmip3/output/CNRM-CERFACS/CNRM-CM5/lgm/monClim/atmos/Aclim/r1i1p1/v20140428/tas/tas_Aclim_CNRM-CM5_lgm_r1i1p1_180001-199912-clim.nc"
midHolocene_file="/data/CMIP/pmip3/output/CNRM-CERFACS/CNRM-CM5/midHolocene/monClim/atmos/Aclim/r1i1p1/v20140428/tas/tas_Aclim_CNRM-CM5_midHolocene_r1i1p1_195001-214912-clim.nc"
piControl_file="/data/CMIP/pmip3/output/CNRM-CERFACS/CNRM-CM5/piControl/monClim/atmos/Aclim/r1i1p1/v20140428/tas/tas_Aclim_CNRM-CM5_piControl_r1i1p1_185001-269912-clim.nc"
rcp85_file="/data/CMIP/cmip5/output1/CNRM-CERFACS/CNRM-CM5/rcp85/mon/atmos/Amon/r1i1p1/v20110930/tas/tas_Amon_CNRM-CM5_rcp85_r1i1p1_205601-210012.nc"
abrupt4xCO2_file="/data/CMIP/cmip5/output1/CNRM-CERFACS/CNRM-CM5/abrupt4xCO2/mon/atmos/Amon/r1i1p1/v20110701/tas/tas_Amon_CNRM-CM5_abrupt4xCO2_r1i1p1_195001-199912.nc"
#lgm
ncra -O $lgm_file CNRM-CM5/tas_Aavg_CNRM-CM5_lgm_r1i1p1.nc
ncks -A -v areacella $area_file CNRM-CM5/tas_Aavg_CNRM-CM5_lgm_r1i1p1.nc
#midHolocene
ncra -O $midHolocene_file CNRM-CM5/tas_Aavg_CNRM-CM5_midHolocene_r1i1p1.nc
ncks -A -v areacella $area_file CNRM-CM5/tas_Aavg_CNRM-CM5_midHolocene_r1i1p1.nc
#piControl
ncra -O $piControl_file CNRM-CM5/tas_Aavg_CNRM-CM5_piControl_r1i1p1.nc
ncks -A -v areacella $area_file CNRM-CM5/tas_Aavg_CNRM-CM5_piControl_r1i1p1.nc
#rcp85
ncra -O -d time,'2081-01-01','2100-12-31' $rcp85_file CNRM-CM5/tas_Aavg_CNRM-CM5_rcp85_r1i1p1_208101-210012.nc
ncks -A -v areacella $area_file CNRM-CM5/tas_Aavg_CNRM-CM5_rcp85_r1i1p1_208101-210012.nc
#abrupt4xCO2
ncra -O -d time,'1980-01-01','1999-12-31' $abrupt4xCO2_file CNRM-CM5/tas_Aavg_CNRM-CM5_abrupt4xCO2_r1i1p1_198101-200012.nc
ncks -A -v areacella $area_file CNRM-CM5/tas_Aavg_CNRM-CM5_abrupt4xCO2_r1i1p1_198101-200012.nc

#####IPSL
area_file="/data/CMIP/cmip5/output1/IPSL/IPSL-CM5A-LR/piControl/fx/atmos/fx/r0i0p0/v20110324/areacella/areacella_fx_IPSL-CM5A-LR_piControl_r0i0p0.nc"
lgm_file="/data/CMIP/pmip3/output/IPSL/IPSL-CM5A-LR/lgm/monClim/atmos/Aclim/r1i1p1/v20140428/tas/tas_Aclim_IPSL-CM5A-LR_lgm_r1i1p1_260101-280012-clim.nc"
midHolocene_file="/data/CMIP/pmip3/output/IPSL/IPSL-CM5A-LR/midHolocene/monClim/atmos/Aclim/r1i1p1/v20140428/tas/tas_Aclim_IPSL-CM5A-LR_midHolocene_r1i1p1_230101-280012-clim.nc"
piControl_file="/data/CMIP/pmip3/output/IPSL/IPSL-CM5A-LR/piControl/monClim/atmos/Aclim/r1i1p1/v20140428/tas/tas_Aclim_IPSL-CM5A-LR_piControl_r1i1p1_180001-279912-clim.nc"
rcp85_file="/data/CMIP/cmip5/IPSL/IPSL-CM5A-LR/rcp85/mon/atmos/Amon/r1i1p1/v20111103/tas/tas_Amon_IPSL-CM5A-LR_rcp85_r1i1p1_200601-230012.nc"
abrupt4xCO2_file="/data/CMIP/cmip5/output1/IPSL/IPSL-CM5A-LR/abrupt4xCO2/mon/atmos/Amon/r1i1p1/v20130506/tas/tas_Amon_IPSL-CM5A-LR_abrupt4xCO2_r1i1p1_185001-204912.nc"
#lgm
ncra -O $lgm_file IPSL-CM5A-LR/tas_Aavg_IPSL-CM5A-LR_lgm_r1i1p1.nc
ncks -A -v areacella $area_file IPSL-CM5A-LR/tas_Aavg_IPSL-CM5A-LR_lgm_r1i1p1.nc
#midHolocene
ncra -O $midHolocene_file IPSL-CM5A-LR/tas_Aavg_IPSL-CM5A-LR_midHolocene_r1i1p1.nc
ncks -A -v areacella $area_file IPSL-CM5A-LR/tas_Aavg_IPSL-CM5A-LR_midHolocene_r1i1p1.nc
#piControl
ncra -O $piControl_file IPSL-CM5A-LR/tas_Aavg_IPSL-CM5A-LR_piControl_r1i1p1.nc
ncks -A -v areacella $area_file IPSL-CM5A-LR/tas_Aavg_IPSL-CM5A-LR_piControl_r1i1p1.nc
#rcp85
ncra -O -d time,'2081-01-01','2100-12-31' $rcp85_file IPSL-CM5A-LR/tas_Aavg_IPSL-CM5A-LR_rcp85_r1i1p1_208101-210012.nc
ncks -A -v areacella $area_file IPSL-CM5A-LR/tas_Aavg_IPSL-CM5A-LR_rcp85_r1i1p1_208101-210012.nc
#abrupt4xCO2
ncra -O -d time,'1981-01-01','2000-12-31' $abrupt4xCO2_file IPSL-CM5A-LR/tas_Aavg_IPSL-CM5A-LR_abrupt4xCO2_r1i1p1_198101-200012.nc
ncks -A -v areacella $area_file IPSL-CM5A-LR/tas_Aavg_IPSL-CM5A-LR_abrupt4xCO2_r1i1p1_198101-200012.nc

#####MIROC
area_file="/data/CMIP/cmip5/output1/MIROC/MIROC-ESM/piControl/fx/atmos/fx/r0i0p0/v20120828/areacella/areacella_fx_MIROC-ESM_piControl_r0i0p0.nc"
lgm_file="/data/CMIP/pmip3/output/MIROC/MIROC-ESM/lgm/monClim/atmos/Aclim/r1i1p1/v20140428/tas/tas_Aclim_MIROC-ESM_lgm_r1i1p1_460001-469912-clim.nc"
midHolocene_file="/data/CMIP/pmip3/output/MIROC/MIROC-ESM/midHolocene/monClim/atmos/Aclim/r1i1p1/v20140428/tas/tas_Aclim_MIROC-ESM_midHolocene_r1i1p1_233001-242912-clim.nc"
piControl_file="/data/CMIP/pmip3/output/MIROC/MIROC-ESM/piControl/monClim/atmos/Aclim/r1i1p1/v20140428/tas/tas_Aclim_MIROC-ESM_piControl_r1i1p1_180001-242912-clim.nc"
rcp85_file="/data/CMIP/cmip5/output1/MIROC/MIROC-ESM/rcp85/mon/atmos/Amon/r1i1p1/v20120710/tas/tas_Amon_MIROC-ESM_rcp85_r1i1p1_200601-210012.nc"
abrupt4xCO2_file="/data/CMIP/cmip5/output1/MIROC/MIROC-ESM/abrupt4xCO2/mon/atmos/Amon/r1i1p1/v20120710/tas/tas_Amon_MIROC-ESM_abrupt4xCO2_r1i1p1_000101-015012.nc"
#lgm
ncra -O $lgm_file MIROC-ESM/tas_Aavg_MIROC-ESM_lgm_r1i1p1.nc
ncks -A -v areacella $area_file MIROC-ESM/tas_Aavg_MIROC-ESM_lgm_r1i1p1.nc
#midHolocene
ncra -O $midHolocene_file MIROC-ESM/tas_Aavg_MIROC-ESM_midHolocene_r1i1p1.nc
ncks -A -v areacella $area_file MIROC-ESM/tas_Aavg_MIROC-ESM_midHolocene_r1i1p1.nc
#piControl
ncra -O $piControl_file MIROC-ESM/tas_Aavg_MIROC-ESM_piControl_r1i1p1.nc
ncks -A -v areacella $area_file MIROC-ESM/tas_Aavg_MIROC-ESM_piControl_r1i1p1.nc
#rcp85
ncra -O -d time,'2081-01-01','2100-12-31' $rcp85_file MIROC-ESM/tas_Aavg_MIROC-ESM_rcp85_r1i1p1_208101-210012.nc
ncks -A -v areacella $area_file MIROC-ESM/tas_Aavg_MIROC-ESM_rcp85_r1i1p1_208101-210012.nc
#abrupt4xCO2
ncra -O -d time,'0131-01-01','0150-12-31' $abrupt4xCO2_file MIROC-ESM/tas_Aavg_MIROC-ESM_abrupt4xCO2_r1i1p1_198101-200012.nc
ncks -A -v areacella $area_file MIROC-ESM/tas_Aavg_MIROC-ESM_abrupt4xCO2_r1i1p1_198101-200012.nc

#####CSIRO
area_file="/data/CMIP/cmip5/output1/CSIRO-QCCCE/CSIRO-Mk3-6-0/piControl/fx/atmos/fx/r0i0p0/v1/areacella/areacella_fx_CSIRO-Mk3-6-0_piControl_r0i0p0.nc"
midHolocene_file="/data/CMIP/pmip3/output/CSIRO-QCCCE/CSIRO-Mk3-6-0/midHolocene/monClim/atmos/Aclim/r1i1p1/v20140428/tas/tas_Aclim_CSIRO-Mk3-6-0_midHolocene_r1i1p1_000101-010012-clim.nc"
piControl_file="/data/CMIP/pmip3/output/CSIRO-QCCCE/CSIRO-Mk3-6-0/piControl/monClim/atmos/Aclim/r1i1p1/v20140428/tas/tas_Aclim_CSIRO-Mk3-6-0_piControl_r1i1p1_000101-050012-clim.nc"
rcp85_file="/data/CMIP/cmip5/output1/CSIRO-QCCCE/CSIRO-Mk3-6-0/rcp85/mon/atmos/Amon/r1i1p1/v1/tas/tas_Amon_CSIRO-Mk3-6-0_rcp85_r1i1p1_200601-210012.nc"
abrupt4xCO2_file="/data/CMIP/cmip5/output1/CSIRO-QCCCE/CSIRO-Mk3-6-0/abrupt4xCO2/mon/atmos/Amon/r1i1p1/v1/tas/tas_Amon_CSIRO-Mk3-6-0_abrupt4xCO2_r1i1p1_000101-015012.nc"
#midHolocene
ncra -O $midHolocene_file CSIRO-Mk3-6-0/tas_Aavg_CSIRO-Mk3-6-0_midHolocene_r1i1p1.nc
ncks -A -v areacella $area_file CSIRO-Mk3-6-0/tas_Aavg_CSIRO-Mk3-6-0_midHolocene_r1i1p1.nc
#piControl
ncra -O $piControl_file CSIRO-Mk3-6-0/tas_Aavg_CSIRO-Mk3-6-0_piControl_r1i1p1.nc
ncks -A -v areacella $area_file CSIRO-Mk3-6-0/tas_Aavg_CSIRO-Mk3-6-0_piControl_r1i1p1.nc
#rcp85
ncra -O -d time,'2081-01-01','2100-12-31' $rcp85_file CSIRO-Mk3-6-0/tas_Aavg_CSIRO-Mk3-6-0_rcp85_r1i1p1_208101-210012.nc
ncks -A -v areacella $area_file CSIRO-Mk3-6-0/tas_Aavg_CSIRO-Mk3-6-0_rcp85_r1i1p1_208101-210012.nc
#abrupt4xCO2
ncra -O -d time,'0131-01-01','0150-12-31' $abrupt4xCO2_file CSIRO-Mk3-6-0/tas_Aavg_CSIRO-Mk3-6-0_abrupt4xCO2_r1i1p1_198101-200012.nc
ncks -A -v areacella $area_file CSIRO-Mk3-6-0/tas_Aavg_CSIRO-Mk3-6-0_abrupt4xCO2_r1i1p1_198101-200012.nc

#####GISS
area_file="/data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/piControl/fx/atmos/fx/r0i0p0/v20130312/areacella/areacella_fx_GISS-E2-R_piControl_r0i0p0.nc"
lgm_file="/data/CMIP/pmip3/output/NASA-GISS/GISS-E2-R/lgm/monClim/atmos/Aclim/r1i1p150/v20140428/tas/tas_Aclim_GISS-E2-R_lgm_r1i1p150_300001-309912-clim.nc"
midHolocene_file="/data/CMIP/pmip3/output/NASA-GISS/GISS-E2-R/midHolocene/monClim/atmos/Aclim/r1i1p1/v20140428/tas/tas_Aclim_GISS-E2-R_midHolocene_r1i1p1_250001-259912-clim.nc"
piControl_file="/data/CMIP/pmip3/output/NASA-GISS/GISS-E2-R/piControl/monClim/atmos/Aclim/r1i1p1/v20140428/tas/tas_Aclim_GISS-E2-R_piControl_r1i1p1_333101-453012-clim.nc"
rcp85_file="/data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/rcp85/mon/atmos/Amon/r1i1p1/v20121016/tas/tas_Amon_GISS-E2-R_rcp85_r1i1p1_207601-210012.nc"
abrupt4xCO2_file="/data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/abrupt4xCO2/mon/atmos/Amon/r1i1p1/v20120808/tas/tas_Amon_GISS-E2-R_abrupt4xCO2_r1i1p1_197601-200012.nc"
#lgm
ncra -O $lgm_file GISS-E2-R/tas_Aavg_GISS-E2-R_lgm_r1i1p1.nc
ncks -A -v areacella $area_file GISS-E2-R/tas_Aavg_GISS-E2-R_lgm_r1i1p1.nc
#midHolocene
ncra -O $midHolocene_file GISS-E2-R/tas_Aavg_GISS-E2-R_midHolocene_r1i1p1.nc
ncks -A -v areacella $area_file GISS-E2-R/tas_Aavg_GISS-E2-R_midHolocene_r1i1p1.nc
#piControl
ncra -O $piControl_file GISS-E2-R/tas_Aavg_GISS-E2-R_piControl_r1i1p1.nc
ncks -A -v areacella $area_file GISS-E2-R/tas_Aavg_GISS-E2-R_piControl_r1i1p1.nc
#rcp85
ncra -O -d time,'2081-01-01','2100-12-31' $rcp85_file GISS-E2-R/tas_Aavg_GISS-E2-R_rcp85_r1i1p1_208101-210012.nc
ncks -A -v areacella $area_file GISS-E2-R/tas_Aavg_GISS-E2-R_rcp85_r1i1p1_208101-210012.nc
#abrupt4xCO2
ncra -O -d time,'1981-01-01','2000-12-31' $abrupt4xCO2_file GISS-E2-R/tas_Aavg_GISS-E2-R_abrupt4xCO2_r1i1p1_198101-200012.nc
ncks -A -v areacella $area_file GISS-E2-R/tas_Aavg_GISS-E2-R_abrupt4xCO2_r1i1p1_198101-200012.nc

