load "$NCL_DIR/common.ncl"
load "ElsamBrierley.functions.ncl"

;Choose what plots to draw
PMIP3_SCATTER=False
TRENDS_SCATTER=True
;Set some options for the PMIP3 Plot
PMIP3_WIDTH_DTDY15N=False
PMIP3_SH=False
;Set some options for the trends plots
TRENDS_EXTENT=False ;else look at NH HC Strength
TRENDS_TIMESERIES=True ;Plots the timeseries.
TRENDS_EqArc_NOT_sic=True ;Plot the Hadley Cell vs the Annual Average Eq.-Pole gradient not Sept Sea Ice.
TRENDS_SIMULATION="merged_long" ;"hist_20yrs","hist_sat","merged","merged_long"
SMOOTH_TIMESERIES=True ; Runs a decadal smoother on HC
;Set some universal flags for behaviour
VERBOSE=False
PRINT_TO_SCREEN=False
PRINT_LATEX_TABLE=False
OUTPUT_TYPE="pdf"

if PMIP3_SCATTER then 
  model_name=(/"GISS-E2-R","MIROC-ESM","CCSM4","CNRM-CM5","IPSL-CM5A-LR","CSIRO-Mk3-6-0"/)
  simulation=(/"piControl","midHolocene","lgm","rcp85","abrupt4xCO2"/)
  Metrics=new((/dimsizes(model_name),dimsizes(simulation),8/),float)
  Metrics=Metrics@_FillValue
  do gcm=0,dimsizes(model_name)-1
    do run=0,dimsizes(simulation)-1
      if .not.((model_name(gcm).eq."CSIRO-Mk3-6-0").and.(simulation(run).eq."lgm")) then
        zmpsi_field=load_calc_interp_zmpsi(model_name(gcm),"Aavg",simulation(run),False,False)
        zmpsi_field=zmpsi_field/10^10
        Metrics(gcm,run,0:4)=tofloat(compute_zmpsi_metrics(zmpsi_field))
        Metrics(gcm,run,5:6)=tofloat(load_compute_tas_gradient(model_name(gcm),"Aavg",simulation(run),-999,VERBOSE))    
        if PMIP3_SH then
          Metrics(gcm,run,7)=tofloat(load_compute_tas_gradient(model_name(gcm),"Aavg",simulation(run),-15,VERBOSE))  
        else
          Metrics(gcm,run,7)=tofloat(load_compute_tas_gradient(model_name(gcm),"Aavg",simulation(run),15,VERBOSE))    
        end if
        delete(zmpsi_field)
      end if
    end do
  end do

  if PRINT_TO_SCREEN then 
    print("  ")
    print("Metrics for Elsam & Brierley - Preind as absolute - others as changes!")
    print(" Run, Model, Max SF, Min SF, SH Boundary, Middle latitude, NH Boundary, Eq-Arctic, Eq-Antarctic,DT/DY at 15oN")
    do gcm=0,dimsizes(model_name)-1
      do run=0,dimsizes(simulation)-1
        if run.eq.0 then
          print(" "+simulation(run)+","+model_name(gcm)+","+Metrics(gcm,run,0)+","+Metrics(gcm,run,1)+\
            ","+Metrics(gcm,run,2)+","+Metrics(gcm,run,3)+","+Metrics(gcm,run,4)+","+\
            Metrics(gcm,run,5)+","+Metrics(gcm,run,6)+","+Metrics(gcm,run,7))
        else
          diffs=Metrics(gcm,run,:)-Metrics(gcm,0,:)
          print(" "+simulation(run)+","+model_name(gcm)+","+diffs(0)+","+diffs(1)+\
            ","+diffs(2)+","+diffs(3)+","+diffs(4)+","+\
            diffs(5)+","+diffs(6)+","+diffs(7))
          delete(diffs)
        end if
      end do
    end do
  end if

  if PRINT_LATEX_TABLE then 
    print("  ")
    print("Metrics for Elsam & Brierley - Preind as absolute - others as changes!")
    print("  ")
    print("\begin{tabular}{|c|c|c|c|c|c|c|c|c|}")
    print("\hline")
    print("\textbf{Model}&\textbf{Run}&\textbf{Max. $\Psi$} ($10^{10} kg/s$)&"+\
            "\textbf{Min. $\Psi$} ($10^{10} kg/s$)&\textbf{S.H. Boundary} (\textdegree{}S)&"+\
            "\textbf{ITCZ} (\textdegree{}N)& \textbf{N.H. Extent} (\textdegree{}N)&"+\
            "\textbf{Eq.-Arc. $\Delta T$} (\textdegree{}C)&"+\
            "\textbf{Eq.-Ant. $\Delta T$} (\textdegree{}C)}\\")
    print("\hline")
    do gcm=0,dimsizes(model_name)-1
      do run=0,dimsizes(simulation)-1
        if run.eq.0 then
          print(" "+model_name(gcm)+"&\textit{"+simulation(run)+\
                "}&\textit{"+sprintf("%5.2f",Metrics(gcm,run,0))+\
                "}&\textit{"+sprintf("%5.2f",Metrics(gcm,run,1))+\
                "}&\textit{"+sprintf("%5.1f",Metrics(gcm,run,2))+\
                "}&\textit{"+sprintf("%5.1f",Metrics(gcm,run,3))+\
                "}&\textit{"+sprintf("%5.1f",Metrics(gcm,run,4))+\
                "}&\textit{"+sprintf("%5.1f",Metrics(gcm,run,5))+\
                "}&\textit{"+sprintf("%5.1f",Metrics(gcm,run,6))+"}\\")
        else
          diffs=Metrics(gcm,run,:)-Metrics(gcm,0,:)
          if run.eq.2.and.gcm.eq.5 then
            print("&lgm&&&&&&&\\")
          else
            print("&"+simulation(run)+"&"+sprintf("%9.2f",diffs(0))+"&"+sprintf("%9.2f",diffs(1))+\
            "&"+sprintf("%9.1f",diffs(2))+"&"+sprintf("%9.1f",diffs(3))+"&"+sprintf("%9.1f",diffs(4))+"&"+\
            sprintf("%9.1f",diffs(5))+"&"+sprintf("%9.1f",diffs(6))+"\\")
          end if
          delete(diffs)
        end if
      end do
      print("\hline")
    end do
    print("\end{tabular}")
  end if
  
  if PMIP3_WIDTH_DTDY15N then
    wks = gsn_open_wks(OUTPUT_TYPE,"Scatter_plot_metrics.PMIP3_WIDTH_DTDY15N")
  else
    wks = gsn_open_wks(OUTPUT_TYPE,"Scatter_plot_metrics.PMIP3")
  end if
  total_anom_vals=dimsizes(model_name)*(dimsizes(simulation)-1)
  xarray=new(total_anom_vals,float)
  yarray=new((/total_anom_vals,total_anom_vals/),float)
  markers=new(total_anom_vals,integer)
  colors=new(total_anom_vals,string)
  gcm_markers=new(dimsizes(model_name),integer)
  gcm_markers(0)=NhlNewMarker(wks,"r",35,0.0,0.0,1,2,0)
  gcm_markers(1)=NhlNewMarker(wks,"y",35,0.0,0.0,1,2,0)
  gcm_markers(2)=NhlNewMarker(wks,"z",35,0.0,0.0,1,2,0)
  gcm_markers(3)=NhlNewMarker(wks,"k",37,0.0,0.0,1,2,0)
  gcm_markers(4)=NhlNewMarker(wks,"l",37,0.0,0.0,1,2,0)
  gcm_markers(5)=NhlNewMarker(wks,"m",37,0.0,0.0,1,2,0)
  run_colors=(/"yellow","black","blue","red","orange"/)
  yarray=yarray@_FillValue
  xarray=xarray@_FillValue
  n=0
  NH_widths=Metrics(:,:,4)-Metrics(:,:,3)
  SH_widths=Metrics(:,:,3)-Metrics(:,:,2)
  do run=1,dimsizes(simulation)-1
    do gcm=0,dimsizes(model_name)-1
      if PMIP3_SH
        if PMIP3_WIDTH_DTDY15N then
          yarray(n,n)=SH_widths(gcm,run)-SH_widths(gcm,0)
          xarray(n)=Metrics(gcm,run,7)-Metrics(gcm,0,7)
        else
          yarray(n,n)=Metrics(gcm,run,0)-Metrics(gcm,0,0)
          xarray(n)=Metrics(gcm,run,6)-Metrics(gcm,0,6)
        end if
      else        
        if PMIP3_WIDTH_DTDY15N then
          yarray(n,n)=NH_widths(gcm,run)-NH_widths(gcm,0)
          xarray(n)=Metrics(gcm,run,7)-Metrics(gcm,0,7)
        else
          yarray(n,n)=Metrics(gcm,run,0)-Metrics(gcm,0,0)
          xarray(n)=Metrics(gcm,run,5)-Metrics(gcm,0,5)
        end if
      end if
      markers(n)=gcm_markers(gcm)
      colors(n)=run_colors(run)
      n=n+1
    end do
  end do

  res=True
  res@gsnYRefLine=0
  res@gsnXRefLine=0
  res@tiMainString="PMIP3 Results"
  if PMIP3_WIDTH_DTDY15N then
    res@tiYAxisString="Change in Extent"
    res@tiXAxisString="Change in Local Temp. Gradient"
  else
    res@tiYAxisString="Change in Streamfunction"
    res@tiXAxisString="Change in Equator to Pole Gradient"
  end if
  res@xyMarkLineMode="Markers"
  res@xyMarkers=markers
  res@xyMarkerColors=colors
  plot=gsn_csm_xy(wks,xarray,yarray,res)
;     NH Cell Width
;      yarray(n,n)=NH_widths(gcm,run)-NH_widths(gcm,0)
;      xarray(n)=Metrics(gcm,run,5)-Metrics(gcm,0,5)
;  res@tiYAxisString="Change in NH Cell Width (deg)"
  delete([/wks,plot,res,model_name,simulation,xarray,yarray,NH_widths,Metrics/])
end if;PMIP3_SCATTER

if TRENDS_SCATTER then 
  ;read in cmip model names (as 43x1 dimension Table)
  if TRENDS_SIMULATION.eq."hist_20yrs" then 
    names=readAsciiTable("synda_datasets/cmip5_historical_modelnames",1,"string",0)
    nyrs=27
    yrs=fspan(1979,2005,nyrs)
  end if
  if TRENDS_SIMULATION.eq."hist_sat" then
    names=readAsciiTable("synda_datasets/cmip5_historical_modelnames",1,"string",0)
    nyrs=20
    yrs=fspan(1981,2000,nyrs)
  end if
  if TRENDS_SIMULATION.eq."merged" then
    names=readAsciiTable("synda_datasets/cmip5merged_modelnames",1,"string",0)
    nyrs=37
    yrs=fspan(1979,2015,nyrs)
  end if
  if TRENDS_SIMULATION.eq."merged_long" then
    names=readAsciiTable("synda_datasets/cmip5merged_modelnames",1,"string",0)
    nyrs=100
    yrs=fspan(1950,2049,nyrs)
  end if
  decades=fspan(0.05,nyrs*0.1-0.05,nyrs)
  trends_array=new((/dimsizes(names(:,0)),2/),float)
  timeseries=new((/dimsizes(names(:,0)),2,nyrs/),float)
  timeseries=(/timeseries@_FillValue/)
  
  if PRINT_LATEX_TABLE then 
    print("  ")
    print("Metrics for Elsam & Brierley - Trends")
    print("  ")
    print("\begin{tabular}{|c|c|c|c|c|}")
    print("\hline")
    print("\textbf{Model}&\textbf{Max. $\Psi$} ($10^{10} kg/s/dec$)&"+\
            "\textbf{N.H. Extent} ($^{\circ}C/dec$)&"+\
            "\textbf{Eq.-Arctic. $\Delta T$} ($^{\circ}C/dec$)&"+\
            "\textbf{Sept. Arctic Sea Ice} ($km^{2}/dec$)}\\")
    print("\hline")
  end if

  do gcm=0,dimsizes(names(:,0))-1
    if VERBOSE then
      print(" "+gcm+" "+names(gcm,0))
    end if
    if PRINT_LATEX_TABLE then
      str=names(gcm,0)+"&"
    end if
    zmpsi_field=load_calc_interp_zmpsi(names(gcm,0),"Aann",TRENDS_SIMULATION,True,VERBOSE)
    zmpsi_field=zmpsi_field/10^10
    Metrics=tofloat(compute_zmpsi_metrics(zmpsi_field))
    if (.not.TRENDS_EXTENT).or.PRINT_LATEX_TABLE then
      timeseries(gcm,1,0:dimsizes(Metrics(:,0))-1)=(/Metrics(:,0)/)
      if SMOOTH_TIMESERIES then
        zmpsiN_rc=regline(decades,runave(timeseries(gcm,1,:),11,0))
      else
        zmpsiN_rc=regline(decades,timeseries(gcm,1,:))
      end if
    end if
    if PRINT_LATEX_TABLE then
      str=str+sprintf("%9.3f",zmpsiN_rc)+"&"
    end if
    if TRENDS_EXTENT.or.PRINT_LATEX_TABLE then
;      timeseries(gcm,1,:)=(/Metrics(:,4)-Metrics(:,3)/)
      timeseries(gcm,1,0:dimsizes(Metrics(:,4))-1)=(/Metrics(:,4)/)
      if SMOOTH_TIMESERIES then
        zmpsiN_rc=regline(decades,runave(timeseries(gcm,1,:),11,0))
      else
        zmpsiN_rc=regline(decades,timeseries(gcm,1,:))
      end if
    end if
    if PRINT_LATEX_TABLE then
      str=str+sprintf("%9.3f",zmpsiN_rc)+"&"
    end if
    trends_array(gcm,1)=(/zmpsiN_rc/)
    delete([/zmpsiN_rc,Metrics,zmpsi_field/])
    if TRENDS_EqArc_NOT_sic.or.PRINT_LATEX_TABLE then
      raw=load_compute_tas_gradient(names(gcm,0),"Aann",TRENDS_SIMULATION,-999,VERBOSE)
      EqArc=raw(0,:)
      timeseries(gcm,0,0:dimsizes(EqArc)-1)=(/EqArc/)
      if SMOOTH_TIMESERIES then
        EqArc_rc=regline(decades,runave(timeseries(gcm,0,:),11,0))
      else
        EqArc_rc=regline(decades,timeseries(gcm,0,:))
      end if
      if PRINT_LATEX_TABLE then
        str=str+sprintf("%9.2f",EqArc_rc)+"&"
      end if
      trends_array(gcm,0)=(/EqArc_rc/)
      delete([/raw,EqArc,EqArc_rc/])
    end if
    if (.not.TRENDS_EqArc_NOT_sic).or.PRINT_LATEX_TABLE then
      sicArc=load_compute_sep_seaice_area(names(gcm,0),TRENDS_SIMULATION,VERBOSE)
      if names(gcm,0).eq."CESM1-WACCM" then
        timeseries(gcm,0,5:dimsizes(sicArc)+4)=(/sicArc/)
      else
        timeseries(gcm,0,0:dimsizes(sicArc)-1)=(/sicArc/)
      end if
      if SMOOTH_TIMESERIES then
        sicArc_rc=regline(decades,runave(timeseries(gcm,0,:),11,0))
      else
        sicArc_rc=regline(decades,timeseries(gcm,0,:))
      end if
      if PRINT_LATEX_TABLE then
        str=str+sprintf("%9.2f",sicArc_rc)+"\\"
        print(" "+str)
      end if
      trends_array(gcm,0)=(/sicArc_rc/)
      delete([/sicArc,sicArc_rc/])
    end if
  end do     
  if PRINT_LATEX_TABLE then
    print("\hline")
    print("\end{tabular}")
  end if

  if TRENDS_TIMESERIES then
    ts_4plot=new((/1+dimsizes(names(:,0)),2,nyrs/),float)
    ts_4plot(1:dimsizes(names(:,0)),:,:)=(/timeseries/)
    ts_4plot(0,:,:)=(/dim_avg_n(timeseries,0)/)
    if SMOOTH_TIMESERIES then
      ts_4plot=runave(ts_4plot,11,0)
    end if
  else
    if SMOOTH_TIMESERIES then
      timeseries(gcm,1,:)=runave(timeseries(gcm,1,:),11,0)
    end if
  end if

  if PRINT_TO_SCREEN then 
    print("  ")
    print("Metrics for Elsam & Brierley - 1979-2015 - Sea-Ice versus NH Boundary")
    if TRENDS_EqArc_NOT_sic then
      if TRENDS_EXTENT then
        print(" Model, Eq-Pole Gradient (~S~o~N~C/dec), NH Poleward Boundary (~S~o~N~/dec)")
      else
        print(" Model, Eq-Pole Gradient (~S~o~N~C/dec), Min SF Change (10^11 /dec)")
      end if
    else
      if TRENDS_EXTENT then
        print(" Model, Sea-ice loss (10^6 km2/dec), NH Poleward Boundary (deg/dec)")
      else
        print(" Model, Sea-ice loss (10^6 km2/dec), Min SF Change (10^11 /dec)")
      end if
    end if
    do gcm=0,dimsizes(names(:,0))-1
      print(" "+names(gcm,0)+","+trends_array(gcm,0)+","+trends_array(gcm,1))
    end do
  end if

  ;compute correlation
  r = escorc(trends_array(:,0),trends_array(:,1))  ; Pearson correlation
  df = dimsizes(names(:,0))-2 ; degrees of freedom
  z = 0.5*log((1+r)/(1-r))  ; Transform to Fischer z-statistic
  se_z   = 1.0/sqrt(df-1)                       ; standard error of z-statistic
  zlow = z - 1.96*se_z                ; 95%  (2.58 for 99%)
  zhi  = z + 1.96*se_z                 
  rlow = (exp(2*zlow)-1)/(exp(2*zlow)+1); inverse z-transform; return to r space (-1 to +1)
  rhi  = (exp(2*zhi )-1)/(exp(2*zhi )+1); inverse z-transform; return to r space (-1 to +1)
  print("Correlation = "+r+" ("+rlow+" to "+rhi+")")

  if TRENDS_TIMESERIES then
    wks = gsn_open_wks(OUTPUT_TYPE,"Scatter_plot_metrics.Trends_timeseries")
    gsn_define_colormap(wks,"hlu_default")
    plots=new(2,graphic)
    res=True
    res@gsnYRefLine=0
    res@gsnXRefLine=0
    res@tiMainString="Timeseries"
    res@tiXAxisString="Year"
    res@gsnDraw=False
    res@gsnFrame=False
    colors=ispan(1,42,1)
    colors(21:40)=colors(1:20)
    res@xyLineColors=colors
    res@xyLineThicknesses=4.
    res@xyLineThicknessF=2.
    dashes=new(41,integer)
    dashes(0:20)=0
    dashes(21:40)=2
    res@xyDashPatterns=dashes
    res@tiXAxisString="Year"
    if TRENDS_EqArc_NOT_sic then
      res@tiYAxisString="Equator to Pole Gradient (~S~o~N~C)"
    else
      res@tiYAxisString="Sept. sea ice extent (10~S~6~N~km~S~2~N~)"
    end if
    res@pmLegendDisplayMode="Always"
    res@pmLegendSide           = "Right"               ; Change location of 
    res@pmLegendWidthF=0.2
    res@pmLegendHeightF=0.6
    label_names=new(41,string)
    label_names(0)="Multi-Model Mean"
    label_names(1:dimsizes(names(:,0)))=names(:,0)
    res@xyExplicitLegendLabels = label_names         ; create explicit labels
    res@lgBoxMinorExtentF = 0.1
    res@lgLabelFontHeightF=0.007	
    plots(0)=gsn_csm_xy(wks,yrs,ts_4plot(:,0,:),res)
    if TRENDS_EXTENT then
      res@tiYAxisString="NH Hadley Cell Edge (~S~o~N~N)"
    else
      res@tiYAxisString="~F33~Y~F21~~B~min~N~ (10~S~10~N~ kg/s)"
    end if
    plots(1)=gsn_csm_xy(wks,yrs,ts_4plot(:,1,:),res)
    pan=True
    pan@gsnMaximize=True
    pan@gsnPanelRowSpec=True
    gsn_panel(wks,plots,(/1,1/),pan)
    delete(wks)
    delete([/res,pan,plots/])
  end if
  

  res=True
  res@gsnYRefLine=0
  res@gsnXRefLine=0
  res@tiMainString="Trends"
  if TRENDS_EqArc_NOT_sic then
    res@tiXAxisString="Equator to Pole Gradient (~S~o~N~C/decade)"
  else
    res@tiXAxisString="Arctic sea ice loss (10~S~6~N~km~S~2~N~/decade)"
  end if
  res@xyMarkLineMode="Markers"
  markers=new(41,integer)
  markers(0:20)=16
  markers(21:40)=4
  res@xyMarkers=markers
  if TRENDS_EXTENT then
    wks = gsn_open_wks(OUTPUT_TYPE,"Scatter_plot_metrics.Trends_extent")
    res@tiYAxisString="N. HC Expansion (~S~o~N~N/decade)"
  else
    wks = gsn_open_wks(OUTPUT_TYPE,"Scatter_plot_metrics.Trends")
    res@tiYAxisString="~F33~Y~F21~~B~max~N~ Changes (10~S~10~N~ kg/s/decade)"
  end if
  gsn_define_colormap(wks,"hlu_default")
  colors=ispan(1,42,1)
  colors(21:40)=colors(1:20)
  res@xyMarkerColors=colors
  plot=gsn_csm_xy(wks,transpose((/trends_array(:,0),trends_array(:,0)/)),\
                      transpose((/trends_array(:,1),trends_array(:,1)/)),res)
  delete([/wks,plot,res,names,trends_array/])
end if
