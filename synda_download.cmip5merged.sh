#!/bin/bash

#This program will hopefully download all necessary files using synda automatically to analsye the cmip historical simulation data

DRYRUN="False"

for dataset in `cat synda_datasets/cmip5merged_hist_Amon`
do
    echo $dataset
    if [ $DRYRUN == "True" ]; then
	synda search $dataset tas
	synda search $dataset ps
	synda search $dataset va
    else
	synda install -y $dataset tas
	synda install -y $dataset ps
	synda install -y $dataset va
   fi
done

for dataset in `cat synda_datasets/cmip5merged_hist_OImon`
do
    echo $dataset
    if [ $DRYRUN == "True" ]; then
        synda search $dataset sic
    else
        synda install -y $dataset sic
    fi
done

for dataset in `cat synda_datasets/cmip5merged_rcp_Amon`
do
    echo $dataset
    if [ $DRYRUN == "True" ]; then
	synda search $dataset tas
	synda search $dataset ps
	synda search $dataset va
    else
	synda install -y $dataset tas
	synda install -y $dataset ps
	synda install -y $dataset va
   fi
done

for dataset in `cat synda_datasets/cmip5merged_rcp_OImon`
do
    echo $dataset
    if [ $DRYRUN == "True" ]; then
        synda search $dataset sic
    else
        synda install -y $dataset sic
    fi
done

for dataset in `cat synda_datasets/cmip5merged_fx_atmos`
do
    echo $dataset
    if [ $DRYRUN == "True" ]; then
        synda search $dataset areacella
    else
        synda install -y $dataset areacella
    fi
done

for dataset in `cat synda_datasets/cmip5merged_fx_ocean`
do
    echo $dataset
    if [ $DRYRUN == "True" ]; then
        synda search $dataset areacello
    else
        synda install -y $dataset areacello
    fi
done
