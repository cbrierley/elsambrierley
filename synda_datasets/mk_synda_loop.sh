#!/bin/bash

# I tried to look at just the historical cmip5 trends and made the file manually.
# This has got a bit too complicated, so I'm going to try to automate it somewhat

# This file will perform a little loop over the pre-defined model names
declare -a modelnames
modelnames=( `cat ~/ElsamBrierley/Scripts/synda_datasets/cmip5merged_modelnames | tr '.' '/'` )
rcp_OImon_outfile=cmip5merged_rcps_OImon
rcp_Amon_outfile=cmip5merged_rcps_Amon
hist_OImon_outfile=cmip5merged_hist_OImon
hist_Amon_outfile=cmip5merged_hist_Amon

rm $rcp_Amon_outfile $rcp_OImon_outfile $hist_Amon_outfile $hist_OImon_outfile

for (( i=0; i<${#modelnames[@]}; i++ ));
do
    echo $i ${modelnames[i]}
    case ${modelnames[i]} in
        "CESM1-WACCM")
	 synda search model=${modelnames[i]} ensemble=r2i1p1 experiment=rcp26,rcp45,rcp60,rcp85 Amon tas va ps cmip5 >> $rcp_Amon_outfile
	 synda search model=${modelnames[i]} ensemble=r2i1p1 experiment=rcp26,rcp45,rcp60,rcp85 OImon sic cmip5 >> $rcp_OImon_outfile
	 synda search model=${modelnames[i]} ensemble=r2i1p1 experiment=historical Amon tas va ps cmip5 >> $hist_Amon_outfile
	 synda search model=${modelnames[i]} ensemble=r2i1p1 experiment=historical OImon sic cmip5 >> $hist_OImon_outfile;;
	*)    
	 synda search model=${modelnames[i]} ensemble=r1i1p1 experiment=rcp26,rcp45,rcp60,rcp85 Amon tas va ps cmip5 >> $rcp_Amon_outfile
	 synda search model=${modelnames[i]} ensemble=r1i1p1 experiment=rcp26,rcp45,rcp60,rcp85 OImon sic cmip5 >> $rcp_OImon_outfile
	 synda search model=${modelnames[i]} ensemble=r1i1p1 experiment=historical Amon tas va ps cmip5 >> $hist_Amon_outfile
	 synda search model=${modelnames[i]} ensemble=r1i1p1 experiment=historical OImon sic cmip5 >> $hist_OImon_outfile;;
    esac
done 

sed -i ':complete  ::g' $rcp_Amon_outfile
sed -i ':complete  ::g' $rcp_OImon_outfile
sed -i ':complete  ::g' $hist_Amon_outfile
sed -i ':complete  ::g' $hist_OImon_outfile
sed -i ':new       ::g' $rcp_Amon_outfile
sed -i ':new       ::g' $rcp_OImon_outfile
sed -i ':new       ::g' $hist_Amon_outfile
sed -i ':new       ::g' $hist_OImon_outfile

#The files still some manual fiddling with in a text editor unfortunately
