#!/bin/bash
#
# This script will create annual mean fields over a set time range from the CMIP5 data.
# Aiming for annual means with the areacella or areacello attached

cd /data/aod/ElsamBrierley_Processed_Files/

NO_OVERWRITE="True" #setting to "True" will prevent the existing files being overwritten [useful for filling holes]
output_dates="1950-2049" 

declare -a atmos_dir atmos_fx ice_dir ocean_fx model_names
case $output_dates in
    "1979-2005")
	start_dt='1979-01-01'
	start_str=197901
	end_dt='2005-12-31'
	end_str=200512
	startSep_dt='1979-09-01'
	endSep_dt='2005-09-30'
	atmos_dir=( `cat ~/ElsamBrierley/Scripts/synda_datasets/cmip5_historical_Amon | tr '.' '/'` )
	atmos_fx=( `cat ~/ElsamBrierley/Scripts/synda_datasets/cmip5_historical_fx_atmos | tr '.' '/'` )
	ice_dir=( `cat ~/ElsamBrierley/Scripts/synda_datasets/cmip5_historical_OImon | tr '.' '/'` )
	ocean_fx=( `cat ~/ElsamBrierley/Scripts/synda_datasets/cmip5_historical_fx_ocean | tr '.' '/'` )
	modelnames=( `cat ~/ElsamBrierley/Scripts/synda_datasets/cmip5_historical_modelnames | tr '.' '/'` )
	middle_file_str="_historical_r1i1p1_";;
    "1981-2000")
	start_dt='1981-01-01'
	start_str=198101
	end_dt='2000-12-31'
	end_str=200012
	startSep_dt='1981-09-01'
	endSep_dt='2000-09-30'
	atmos_dir=( `cat ~/ElsamBrierley/Scripts/synda_datasets/cmip5_historical_Amon | tr '.' '/'` )
	atmos_fx=( `cat ~/ElsamBrierley/Scripts/synda_datasets/cmip5_historical_fx_atmos | tr '.' '/'` )
	ice_dir=( `cat ~/ElsamBrierley/Scripts/synda_datasets/cmip5_historical_OImon | tr '.' '/'` )
	ocean_fx=( `cat ~/ElsamBrierley/Scripts/synda_datasets/cmip5_historical_fx_ocean | tr '.' '/'` )
	modelnames=( `cat ~/ElsamBrierley/Scripts/synda_datasets/cmip5_historical_modelnames | tr '.' '/'` )
	middle_file_str="_historical_r1i1p1_";;
    "1979-2015")
	start_dt='1979-01-01'
	start_str=197901
	end_dt='2015-12-31'
	end_str=201512
	startSep_dt='1979-09-01'
	endSep_dt='2015-09-30'
	startSep_WACCM_dt='1980-09-01'
	startSep_dt_offset='1980-09-01'
	endSep_dt_offset='2016-09-30'
	declare -a atmos_hist atmos_rcps ice_hist ice_rcps
	atmos_hist=( `cat ~/ElsamBrierley/Scripts/synda_datasets/cmip5merged_hist_Amon | tr '.' '/'` )
	atmos_rcps=( `cat ~/ElsamBrierley/Scripts/synda_datasets/cmip5merged_rcps_Amon | tr '.' '/'` )
	atmos_fx=( `cat ~/ElsamBrierley/Scripts/synda_datasets/cmip5merged_fx_atmos | tr '.' '/'` )
	ice_hist=( `cat ~/ElsamBrierley/Scripts/synda_datasets/cmip5merged_hist_OImon | tr '.' '/'` )
	ice_rcps=( `cat ~/ElsamBrierley/Scripts/synda_datasets/cmip5merged_rcps_OImon | tr '.' '/'` )
	ocean_fx=( `cat ~/ElsamBrierley/Scripts/synda_datasets/cmip5merged_fx_ocean | tr '.' '/'` )
	modelnames=( `cat ~/ElsamBrierley/Scripts/synda_datasets/cmip5merged_modelnames | tr '.' '/'` )
	middle_file_str="_merged_r1i1p1_";;
    "1950-2049")
	start_dt='1950-01-01'
	start_str=195001
	end_dt='2049-12-31'
	end_str=204912
	startSep_dt='1950-09-01'
	startSep_dt_WACCM='1956-09-01'
	endSep_dt='2049-09-30'
	startSep_dt_offset='1951-09-01'
	endSep_dt_offset='2050-09-30'
	declare -a atmos_hist atmos_rcps ice_hist ice_rcps
	atmos_hist=( `cat ~/ElsamBrierley/Scripts/synda_datasets/cmip5merged_hist_Amon | tr '.' '/'` )
	atmos_rcps=( `cat ~/ElsamBrierley/Scripts/synda_datasets/cmip5merged_rcps_Amon | tr '.' '/'` )
	atmos_fx=( `cat ~/ElsamBrierley/Scripts/synda_datasets/cmip5merged_fx_atmos | tr '.' '/'` )
	ice_hist=( `cat ~/ElsamBrierley/Scripts/synda_datasets/cmip5merged_hist_OImon | tr '.' '/'` )
	ice_rcps=( `cat ~/ElsamBrierley/Scripts/synda_datasets/cmip5merged_rcps_OImon | tr '.' '/'` )
	ocean_fx=( `cat ~/ElsamBrierley/Scripts/synda_datasets/cmip5merged_fx_ocean | tr '.' '/'` )
	modelnames=( `cat ~/ElsamBrierley/Scripts/synda_datasets/cmip5merged_modelnames | tr '.' '/'` )
	middle_file_str="_merged_r1i1p1_";;
    *)
	echo UNKNOWN DATE STRING SELECTED
	exit
esac

for (( i=0; i<${#modelnames[@]}; i++ ));
do
    echo $i ${modelnames[i]}
    if [ ! -d ${modelnames[i]} ]; then mkdir ${modelnames[i]}; fi
    #Temperatures
    outf_name=${modelnames[i]}/tas_Aann_${modelnames[i]}$middle_file_str$start_str-$end_str.nc
    if [ ! -f $outf_name ] || [ ! $NO_OVERWRITE == "True" ]; then
	echo Creating $outf_name
	case $output_dates in
	    "1979-2015"|"1950-2049")
            #Temperatures - being more explicit about catting the two runs.
	    ncra -O --mro -d time,$start_dt,'2005-12-31',12,12 -v tas /data/CMIP/${atmos_hist[i]}/tas/*.nc /tmp/cmip5_hist.tas.nc
	    ncra -O --mro -d time,'2006-01-01',$end_dt,12,12 -v tas /data/CMIP/${atmos_rcps[i]}/tas/*.nc /tmp/cmip5_rcps.tas.nc
	    ncrcat -O /tmp/cmip5_hist.tas.nc /tmp/cmip5_rcps.tas.nc $outf_name
	    ncks -A -v areacella /data/CMIP/${atmos_fx[i]}/areacella/*.nc $outf_name;;
	    *)
            #Temperatures
	    ncra -O --mro -d time,$start_dt,$end_dt,12,12 -v tas /data/CMIP/${atmos_dir[i]}/tas/*.nc $outf_name
	    ncks -A -v areacella /data/CMIP/${atmos_fx[i]}/areacella/*.nc $outf_name;;
	esac
    fi
    #Sea ice
    outf_name=${modelnames[i]}/sic_OIsep_${modelnames[i]}$middle_file_str$start_str-$end_str.nc
    if [ ! -f $outf_name ] || [ ! $NO_OVERWRITE == "True" ]; then
	echo Creating $outf_name
	case $output_dates in
	    "1979-2015"|"1950-2049")
	    case ${modelnames[i]} in
		"CanESM2"|"CSIRO-Mk3-6-0"|"GISS-E2-H-CC"|"GISS-E2-R-CC"|"GISS-E2-R") #ice on atmosphere grid not ocean...
		    ncra -O --mro -d time,$startSep_dt,'2005-09-30',12,1 /data/CMIP/${ice_hist[i]}/sic/*.nc /tmp/cmip5_hist.sic.nc
		    ncra -O --mro -d time,'2006-09-01',$endSep_dt,12,1 /data/CMIP/${ice_rcps[i]}/sic/*.nc /tmp/cmip5_rcps.sic.nc
		    ncrcat -O /tmp/cmip5_hist.sic.nc /tmp/cmip5_rcps.sic.nc $outf_name
		    ncks -A -v areacella /data/CMIP/${atmos_fx[i]}/areacella/*.nc  $outf_name
            	    ;;
		"BNU-ESM")
		    ncra -O --mro -d time,$startSep_dt,$endSep_dt,12,1 /data/CMIP/${ice_hist[i]}/sic/*.nc /data/CMIP/${ice_rcps[i]}/sic/*.nc $outf_name
		    ncks -O -v areacello /data/CMIP/${ocean_fx[i]}/areacello/*.nc /tmp/tmp.nc 
		    ncrename -d lat,j -d lon,i /tmp/tmp.nc
		    ncks -A -v areacello /tmp/tmp.nc $outf_name
		    rm /tmp/tmp.nc
		    ;;
                "CCSM4"|"CESM1-BGC"|"CESM1-CAM5")
                    #the dates in these files are confused by refering to year 0. Need to add a year
                    ncra -O --mro -d time,$startSep_dt_offset,'2006-09-30',12,1 /data/CMIP/${ice_hist[i]}/sic/*.nc /tmp/cmip5_hist.sic.nc
                    ncra -O --mro -d time,'2007-09-01',$endSep_dt_offset,12,1 /data/CMIP/${ice_rcps[i]}/sic/*.nc /tmp/cmip5_rcps.sic.nc
                    ncrcat -O /tmp/cmip5_hist.sic.nc /tmp/cmip5_rcps.sic.nc $outf_name
                    ncks -A -v areacello /data/CMIP/${ocean_fx[i]}/areacello/*.nc $outf_name
                    ;;
                "CESM1-WACCM")
                    #the dates in these files are confused by refering to year 0. Need to add a year. Also WACCM only starts in 1955
                    ncra -O --mro -d time,$startSep_dt_WACCM,'2006-09-30',12,1 /data/CMIP/${ice_hist[i]}/sic/*.nc /tmp/cmip5_hist.sic.nc
                    ncra -O --mro -d time,'2007-09-01',$endSep_dt_offset,12,1 /data/CMIP/${ice_rcps[i]}/sic/*.nc /tmp/cmip5_rcps.sic.nc
                    ncrcat -O /tmp/cmip5_hist.sic.nc /tmp/cmip5_rcps.sic.nc $outf_name
                    ncks -A -v areacello /data/CMIP/${ocean_fx[i]}/areacello/*.nc $outf_name
                    ;;
		*)
		    ncra -O --mro -d time,$startSep_dt,'2005-09-30',12,1 /data/CMIP/${ice_hist[i]}/sic/*.nc /tmp/cmip5_hist.sic.nc
		    ncra -O --mro -d time,'2006-09-01',$endSep_dt,12,1 /data/CMIP/${ice_rcps[i]}/sic/*.nc /tmp/cmip5_rcps.sic.nc
		    ncrcat -O /tmp/cmip5_hist.sic.nc /tmp/cmip5_rcps.sic.nc $outf_name
		    ncks -A -v areacello /data/CMIP/${ocean_fx[i]}/areacello/*.nc $outf_name
                    ;;
	    esac ;;
	    *)
		ncra -O --mro -d time,$startSep_dt,$endSep_dt,12,1 /data/CMIP/${ice_dir[i]}/sic/*.nc $outf_name
		case ${modelnames[i]} in
		    "CanESM2"|"CSIRO-Mk3-6-0"|"GISS-E2-H-CC"|"GISS-E2-R-CC"|"GISS-E2-R")
			ncks -A -v areacella /data/CMIP/${atmos_fx[i]}/areacella/*.nc $outf_name;;
		"BNU-ESM")
		    ncks -O -v areacello /data/CMIP/${ocean_fx[i]}/areacello/*.nc /tmp/tmp.nc 
		    ncrename -d lat,j -d lon,i /tmp/tmp.nc
		    ncks -A -v areacello /tmp/tmp.nc $outf_name
		    rm /tmp/tmp.nc;;
		*)
		    ncks -A -v areacello /data/CMIP/${ocean_fx[i]}/areacello/*.nc $outf_name;;
		esac ;;
	esac
    fi #end of sea ice.
    #Velocities and pressures. Some models have them on different grids...
    case ${modelnames[i]} in
	"ACCESS1-0"|"ACCESS1-3"|"HadCM3"|"HadGEM2-CC"|"HadGEM2-ES"|"GISS-E2-H-CC"|"GISS-E2-R-CC"|"GISS-E2-R"|"HadGEM2-AO")
	    #pressure on its own grid first 
	    outf_name=${modelnames[i]}/ps_Aann_${modelnames[i]}$middle_file_str$start_str-$end_str.nc    
	    if [ ! -f $outf_name ] || [ ! $NO_OVERWRITE == "True" ]; then
 	        echo Creating $outf_name
 		case $output_dates in
		    "1979-2015"|"1950-2049")
			ncra -O --mro -d time,$start_dt,'2005-12-31',12,12 -v ps /data/CMIP/${atmos_hist[i]}/ps/*.nc /tmp/cmip5_hist.ps.nc
			ncra -O --mro -d time,'2006-01-01',$end_dt,12,12 -v ps /data/CMIP/${atmos_rcps[i]}/ps/*.nc /tmp/cmip5_rcps.ps.nc
			ncrcat -O /tmp/cmip5_hist.ps.nc /tmp/cmip5_rcps.ps.nc $outf_name
			ncks -A -v areacella /data/CMIP/${atmos_fx[i]}/areacella/*.nc $outf_name;;
		    *)
			ncra -A --mro -d time,$start_dt,$end_dt,12,12 -v ps /data/CMIP/${atmos_dir[i]}/ps/*.nc $outf_name
			ncks -A -v areacella /data/CMIP/${atmos_fx[i]}/areacella/*.nc $outf_name;;
		esac
	    fi
	    #velocity on its own grid second 
	    outf_name=${modelnames[i]}/va_Aann_${modelnames[i]}$middle_file_str$start_str-$end_str.nc    
	    if [ ! -f $outf_name ] || [ ! $NO_OVERWRITE == "True" ]; then
	  	echo Creating $outf_name
 		case $output_dates in
		    "1979-2015"|"1950-2049")
			ncra -O --mro -d time,$start_dt,'2005-12-31',12,12 -v va /data/CMIP/${atmos_hist[i]}/va/*.nc /tmp/cmip5_hist.va.nc
			ncra -O --mro -d time,'2006-01-01',$end_dt,12,12 -v va /data/CMIP/${atmos_rcps[i]}/va/*.nc /tmp/cmip5_rcps.va.nc
			ncrcat -O /tmp/cmip5_hist.va.nc /tmp/cmip5_rcps.va.nc $outf_name;;
		    *)
			ncra -A --mro -d time,$start_dt,$end_dt,12,12 -v va /data/CMIP/${atmos_dir[i]}/va/*.nc $outf_name;;
		esac
	    fi
	    ;;
	*)
	    #velocities and pressures on the same grid. So combined file
	    outf_name=${modelnames[i]}/vaps_Aann_${modelnames[i]}$middle_file_str$start_str-$end_str.nc
	    if [ ! -f $outf_name ] || [ ! $NO_OVERWRITE == "True" ]; then
              	echo Creating $outf_name
 		case $output_dates in
		    "1979-2015"|"1950-2049")
			ncra -O --mro -d time,$start_dt,'2005-12-31',12,12 -v va /data/CMIP/${atmos_hist[i]}/va/*.nc /tmp/cmip5_hist.va.nc
			ncra -O --mro -d time,$start_dt,'2005-12-31',12,12 -v ps /data/CMIP/${atmos_hist[i]}/ps/*.nc /tmp/cmip5_hist.ps.nc
			ncra -O --mro -d time,'2006-01-01',$end_dt,12,12 -v va /data/CMIP/${atmos_rcps[i]}/va/*.nc /tmp/cmip5_rcps.va.nc
			ncra -O --mro -d time,'2006-01-01',$end_dt,12,12 -v ps /data/CMIP/${atmos_rcps[i]}/ps/*.nc /tmp/cmip5_rcps.ps.nc
			ncrcat -O /tmp/cmip5_hist.va.nc /tmp/cmip5_rcps.va.nc $outf_name
			ncrcat -A /tmp/cmip5_hist.ps.nc /tmp/cmip5_rcps.ps.nc $outf_name
  			ncks -A -v areacella /data/CMIP/${atmos_fx[i]}/areacella/*.nc $outf_name;;
		    *)
    			ncra -O --mro -d time,$start_dt,$end_dt,12,12 -v va /data/CMIP/${atmos_dir[i]}/va/*.nc $outf_name
    			ncra -A --mro -d time,$start_dt,$end_dt,12,12 -v ps /data/CMIP/${atmos_dir[i]}/ps/*.nc $outf_name
    			ncks -A -v areacella /data/CMIP/${atmos_fx[i]}/areacella/*.nc $outf_name ;;
		esac
	    fi
	    ;;
    esac
done
