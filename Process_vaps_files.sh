#!/bin/bash
#
# This long script will use the NetCDF Operators (NCO) to create some useful smaller files of the meridional velocities and surface pressures

cd /data/aod/ElsamBrierley_Processed_Files/

#CCSM4 historical
hist_dir=/data/CMIP/cmip5/output1/NCAR/CCSM4/historical/mon/atmos/Amon/r1i1p1/v20130425
ncra -O -d time,'1981-01-01','2000-12-31' -v va $hist_dir/va/va_Amon_CCSM4_historical_r1i1p1_195001-200512.nc CCSM4/vaps_Aavg_CCSM4_historical_r1i1p1_198101-200012.nc
ncra -A -d time,'1981-01-01','2000-12-31' -v ps $hist_dir/ps/ps_Amon_CCSM4_historical_r1i1p1_185001-200512.nc CCSM4/vaps_Aavg_CCSM4_historical_r1i1p1_198101-200012.nc
pi_dir=/home/zcfakle/DATA/Downloads/
ncra -O -v va $pi_dir/va_Amon_CCSM4_piControl_r1i1p1_*.nc CCSM4/vaps_Aavg_CCSM4_piControl_r1i1p1.nc
ncra -A -v ps $pi_dir/ps_Amon_CCSM4_piControl_r1i1p1_*.nc CCSM4/vaps_Aavg_CCSM4_piControl_r1i1p1.nc
ncra -O -d time,'1981-01-01','2000-12-31' $pi_dir/va_Amon_CCSM4_abrupt4xCO2_r1i1p1_195001-200012.nc CCSM4/vaps_Aavg_CCSM4_abrupt4xCO2_r1i1p1_198101-200012.nc
ncra -A -d time,'1981-01-01','2000-12-31' $pi_dir/ps_Amon_CCSM4_abrupt4xCO2_r1i1p1_185001-200012.nc CCSM4/vaps_Aavg_CCSM4_abrupt4xCO2_r1i1p1_198101-200012.nc
lgm_dir=/home/zcfakle/DATA/Dissertation/CMIP5_data/CCSM4
ncra -O -v va $lgm_dir/va_Amon_CCSM4_lgm_r1i1p1_Average.nc CCSM4/vaps_Aavg_CCSM4_lgm_r1i1p1.nc
ncra -A -v ps $lgm_dir/ps_Amon_CCSM4_lgm_r1i1p1_180001-190012.nc CCSM4/vaps_Aavg_CCSM4_lgm_r1i1p1.nc
ncra -O -v va $lgm_dir/va_Amon_CCSM4_midHolocene_r1i1pi_Average.nc CCSM4/vaps_Aavg_CCSM4_midHolocene_r1i1p1.nc
ncra -A -v ps $lgm_dir/ps_Amon_CCSM4_midHolocene_r1i1p1_100001-130012.nc CCSM4/vaps_Aavg_CCSM4_midHolocene_r1i1p1.nc
ncra -O -d time,'2081-01-01','2100-12-31' -v va $lgm_dir/va_Amon_CCSM4_rcp85_r1i1p1_*.nc CCSM4/vaps_Aavg_CCSM4_rcp85_r1i1p1_208101-210012.nc
ncra -A -d time,'2081-01-01','2100-12-31' -v ps $lgm_dir/ps_Amon_CCSM4_rcp85_r1i1p1_*.nc CCSM4/vaps_Aavg_CCSM4_rcp85_r1i1p1_208101-210012.nc

#CNRM-CM5 piControl
hist_dir=/data/CMIP/cmip5/CNRM-CERFACS/CNRM-CM5/historical/mon/atmos/Amon/r1i1p1/v20110901
ncra -O -d time,'1981-01-01','2000-12-31' -v va $hist_dir/va/va_Amon_CNRM-CM5_historical_r1i1p1_195001-200512.nc CNRM-CM5/vaps_Aavg_CNRM-CM5_historical_r1i1p1_198101-200012.nc
ncra -A -d time,'1981-01-01','2000-12-31' -v ps $hist_dir/ps/ps_Amon_CNRM-CM5_historical_r1i1p1_195001-200512.nc CNRM-CM5/vaps_Aavg_CNRM-CM5_historical_r1i1p1_198101-200012.nc
katy_dir=/home/zcfakle/DATA/Downloads/
ncra -O -v va $katy_dir/va_Amon_CNRM-CM5_piControl_r1i1p1_*.nc CNRM-CM5/vaps_Aavg_CNRM-CM5_piControl_r1i1p1.nc
ncra -A -v ps $katy_dir/ps_Amon_CNRM-CM5_piControl_r1i1p1_*.nc CNRM-CM5/vaps_Aavg_CNRM-CM5_piControl_r1i1p1.nc
ncra -O -d time,'1980-01-01','1999-12-31' $katy_dir/va_Amon_CNRM-CM5_abrupt4xCO2_r1i1p1_195001-199912.nc CNRM-CM5/vaps_Aavg_CNRM-CM5_abrupt4xCO2_r1i1p1_198101-200012.nc
ncra -A -d time,'1980-01-01','1999-12-31' $katy_dir/ps_Amon_CNRM-CM5_abrupt4xCO2_r1i1p1_195001-199912.nc CNRM-CM5/vaps_Aavg_CNRM-CM5_abrupt4xCO2_r1i1p1_198101-200012.nc
ncra -O -v va $katy_dir/va_Amon_CNRM-CM5_lgm_r1i1p1_*.nc CNRM-CM5/vaps_Aavg_CNRM-CM5_lgm_r1i1p1.nc
ncra -A -v ps $katy_dir/ps_Amon_CNRM-CM5_lgm_r1i1p1_*.nc CNRM-CM5/vaps_Aavg_CNRM-CM5_lgm_r1i1p1.nc
ncra -O -v va $katy_dir/va_Amon_CNRM-CM5_midHolocene_r1i1p1_*.nc CNRM-CM5/vaps_Aavg_CNRM-CM5_midHolocene_r1i1p1.nc
ncra -A -v ps $katy_dir/ps_Amon_CNRM-CM5_midHolocene_r1i1p1_*.nc CNRM-CM5/vaps_Aavg_CNRM-CM5_midHolocene_r1i1p1.nc
ncra -O -d time,'2081-01-01','2100-12-31' -v va $katy_dir/va_Amon_CNRM-CM5_rcp85_r1i1p1_*.nc CNRM-CM5/vaps_Aavg_CNRM-CM5_rcp85_r1i1p1_208101-210012.nc
ncra -A -d time,'2081-01-01','2100-12-31' -v ps $katy_dir/ps_Amon_CNRM-CM5_rcp85_r1i1p1_*.nc CNRM-CM5/vaps_Aavg_CNRM-CM5_rcp85_r1i1p1_208101-210012.nc

#CSIRO-Mk3-6-0 piControl
hist_dir=/data/CMIP/cmip5/output1/CSIRO-QCCCE/CSIRO-Mk3-6-0/historical/mon/atmos/Amon/r1i1p1/v1/
ncra -O -d time,'1981-01-01','2000-12-31' -v va $hist_dir/va/va_Amon_CSIRO-Mk3-6-0_historical_r1i1p1_*.nc CSIRO-Mk3-6-0/vaps_Aavg_CSIRO-Mk3-6-0_historical_r1i1p1_198101-200012.nc
ncra -A -d time,'1981-01-01','2000-12-31' -v ps $hist_dir/ps/ps_Amon_CSIRO-Mk3-6-0_historical_r1i1p1_*.nc CSIRO-Mk3-6-0/vaps_Aavg_CSIRO-Mk3-6-0_historical_r1i1p1_198101-200012.nc
katy_dir=/home/zcfakle/DATA/Downloads/
ncra -O -v va $katy_dir/va_Amon_CSIRO-Mk3-6-0_piControl_r1i1p1_*.nc CSIRO-Mk3-6-0/vaps_Aavg_CSIRO-Mk3-6-0_piControl_r1i1p1.nc
ncra -A -v ps $katy_dir/ps_Amon_CSIRO-Mk3-6-0_piControl_r1i1p1_*.nc CSIRO-Mk3-6-0/vaps_Aavg_CSIRO-Mk3-6-0_piControl_r1i1p1.nc
ncra -O -d time,'0131-01-01','0150-12-31' $katy_dir/va_Amon_CSIRO-Mk3-6-0_abrupt4xCO2_r1i1p1_*.nc CSIRO-Mk3-6-0/vaps_Aavg_CSIRO-Mk3-6-0_abrupt4xCO2_r1i1p1_198101-200012.nc
ncra -A -d time,'0131-01-01','0150-12-31' $katy_dir/ps_Amon_CSIRO-Mk3-6-0_abrupt4xCO2_r1i1p1_*.nc CSIRO-Mk3-6-0/vaps_Aavg_CSIRO-Mk3-6-0_abrupt4xCO2_r1i1p1_198101-200012.nc
ncra -O -v va $katy_dir/va_Amon_CSIRO-Mk3-6-0_midHolocene_r1i1p1_*.nc CSIRO-Mk3-6-0/vaps_Aavg_CSIRO-Mk3-6-0_midHolocene_r1i1p1.nc
ncra -A -v ps $katy_dir/ps_Amon_CSIRO-Mk3-6-0_midHolocene_r1i1p1_*.nc CSIRO-Mk3-6-0/vaps_Aavg_CSIRO-Mk3-6-0_midHolocene_r1i1p1.nc
ncra -O -d time,'2081-01-01','2100-12-31' -v va $katy_dir/va_Amon_CSIRO-Mk3-6-0_rcp85_r1i1p1_*.nc CSIRO-Mk3-6-0/vaps_Aavg_CSIRO-Mk3-6-0_rcp85_r1i1p1_208101-210012.nc
ncra -A -d time,'2081-01-01','2100-12-31' -v ps $katy_dir/ps_Amon_CSIRO-Mk3-6-0_rcp85_r1i1p1_*.nc CSIRO-Mk3-6-0/vaps_Aavg_CSIRO-Mk3-6-0_rcp85_r1i1p1_208101-210012.nc

#IPSL
hist_dir=/data/CMIP/cmip5/output1/IPSL/IPSL-CM5A-LR/historical/mon/atmos/Amon/r1i1p1/v20110406/
ncra -O -d time,'1981-01-01','2000-12-31' -v va $hist_dir/va/va_Amon_IPSL-CM5A-LR_historical_r1i1p1_195001-200512.nc IPSL-CM5A-LR/vaps_Aavg_IPSL-CM5A-LR_historical_r1i1p1_198101-200012.nc
ncra -A -d time,'1981-01-01','2000-12-31' -v ps $hist_dir/ps/ps_Amon_IPSL-CM5A-LR_historical_r1i1p1_185001-200512.nc IPSL-CM5A-LR/vaps_Aavg_IPSL-CM5A-LR_historical_r1i1p1_198101-200012.nc
katy_dir=/home/zcfakle/DATA/Downloads/
ncra -O -v va $katy_dir/va_Amon_IPSL-CM5A-LR_piControl_r1i1p1_*.nc IPSL-CM5A-LR/vaps_Aavg_IPSL-CM5A-LR_piControl_r1i1p1.nc
ncra -A -v ps $katy_dir/ps_Amon_IPSL-CM5A-LR_piControl_r1i1p1_*.nc IPSL-CM5A-LR/vaps_Aavg_IPSL-CM5A-LR_piControl_r1i1p1.nc
ncra -O -d time,'1981-01-01','2000-12-31' $katy_dir/va_Amon_IPSL-CM5A-LR_abrupt4xCO2_r1i1p1_195001-204912.nc IPSL-CM5A-LR/vaps_Aavg_IPSL-CM5A-LR_abrupt4xCO2_r1i1p1_198101-200012.nc
ncra -A -d time,'1981-01-01','2000-12-31' $katy_dir/ps_Amon_IPSL-CM5A-LR_abrupt4xCO2_r1i1p1_185001-204912.nc IPSL-CM5A-LR/vaps_Aavg_IPSL-CM5A-LR_abrupt4xCO2_r1i1p1_198101-200012.nc
ncra -O -v va $katy_dir/va_Amon_IPSL-CM5A-LR_lgm_r1i1p1_*.nc IPSL-CM5A-LR/vaps_Aavg_IPSL-CM5A-LR_lgm_r1i1p1.nc
ncra -A -v ps $katy_dir/ps_Amon_IPSL-CM5A-LR_lgm_r1i1p1_*.nc IPSL-CM5A-LR/vaps_Aavg_IPSL-CM5A-LR_lgm_r1i1p1.nc
ncra -O -v va $katy_dir/va_Amon_IPSL-CM5A-LR_midHolocene_r1i1pi_*.nc IPSL-CM5A-LR/vaps_Aavg_IPSL-CM5A-LR_midHolocene_r1i1p1.nc
ncra -A -v ps $katy_dir/ps_Amon_IPSL-CM5A-LR_midHolocene_r1i1p1_*.nc IPSL-CM5A-LR/vaps_Aavg_IPSL-CM5A-LR_midHolocene_r1i1p1.nc
ncra -O -d time,'2081-01-01','2100-12-31' -v va $katy_dir/va_Amon_IPSL-CM5A-LR_rcp85_r1i1p1_200601-210512.nc IPSL-CM5A-LR/vaps_Aavg_IPSL-CM5A-LR_rcp85_r1i1p1_208101-210012.nc
ncra -A -d time,'2081-01-01','2100-12-31' -v ps $katy_dir/ps_Amon_IPSL-CM5A-LR_rcp85_r1i1p1_*.nc IPSL-CM5A-LR/vaps_Aavg_IPSL-CM5A-LR_rcp85_r1i1p1_208101-210012.nc

#GISS
hist_dir=/data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/historical/mon/atmos/Amon/r1i1p1/v20121015/
ncra -O -d time,'1981-01-01','2000-12-31' -v va $hist_dir/va/va_Amon_GISS-E2-R_historical_r1i1p1_*.nc GISS-E2-R/va_Aavg_GISS-E2-R_historical_r1i1p1_198101-200012.nc
ncra -A -d time,'1981-01-01','2000-12-31' -v ps $hist_dir/ps/ps_Amon_GISS-E2-R_historical_r1i1p1_*.nc GISS-E2-R/ps_Aavg_GISS-E2-R_historical_r1i1p1_198101-200012.nc 
katy_dir=/home/zcfakle/DATA/Downloads/
ncra -O -v va $katy_dir/va_Amon_GISS-E2-R_piControl_r1i1p1_*.nc GISS-E2-R/va_Aavg_GISS-E2-R_piControl_r1i1p1.nc
ncra -O -v ps $katy_dir/ps_Amon_GISS-E2-R_piControl_r1i1p1_*.nc GISS-E2-R/ps_Aavg_GISS-E2-R_piControl_r1i1p1.nc
ncra -O -d time,'1981-01-01','2000-12-31' $katy_dir/va_Amon_GISS-E2-R_abrupt4xCO2_r1i1p1_*.nc GISS-E2-R/va_Aavg_GISS-E2-R_abrupt4xCO2_r1i1p1_198101-200012.nc
ncra -O -d time,'1981-01-01','2000-12-31' $katy_dir/ps_Amon_GISS-E2-R_abrupt4xCO2_r1i1p1_*.nc GISS-E2-R/ps_Aavg_GISS-E2-R_abrupt4xCO2_r1i1p1_198101-200012.nc
ncra -O -v va $katy_dir/va_Amon_GISS-E2-R_lgm_r1i1p150_*.nc GISS-E2-R/va_Aavg_GISS-E2-R_lgm_r1i1p1.nc
ncra -O -v ps $katy_dir/ps_Amon_GISS-E2-R_lgm_r1i1p150_*.nc GISS-E2-R/ps_Aavg_GISS-E2-R_lgm_r1i1p1.nc
ncra -O -v va $katy_dir/va_Amon_GISS-E2-R_midHolocene_r1i1p1_*.nc GISS-E2-R/va_Aavg_GISS-E2-R_midHolocene_r1i1p1.nc
ncra -O -v ps $katy_dir/ps_Amon_GISS-E2-R_midHolocene_r1i1p1_*.nc GISS-E2-R/ps_Aavg_GISS-E2-R_midHolocene_r1i1p1.nc
ncra -O -d time,'2081-01-01','2100-12-31' -v va $katy_dir/va_Amon_GISS-E2-R_rcp85_r1i1p1_*.nc GISS-E2-R/va_Aavg_GISS-E2-R_rcp85_r1i1p1_208101-210012.nc
ncra -O -d time,'2081-01-01','2100-12-31' -v ps $katy_dir/ps_Amon_GISS-E2-R_rcp85_r1i1p1_*.nc GISS-E2-R/ps_Aavg_GISS-E2-R_rcp85_r1i1p1_208101-210012.nc

#MIROC
hist_dir=/data/CMIP/cmip5/output1/MIROC/MIROC-ESM/historical/mon/atmos/Amon/r1i1p1/v20120710/
ncra -O -d time,'1981-01-01','2000-12-31' -v va $hist_dir/va/va_Amon_MIROC-ESM_historical_r1i1p1_*.nc MIROC-ESM/vaps_Aavg_MIROC-ESM_historical_r1i1p1_198101-200012.nc
ncra -A -d time,'1981-01-01','2000-12-31' -v ps $hist_dir/ps/ps_Amon_MIROC-ESM_historical_r1i1p1_*.nc MIROC-ESM/vaps_Aavg_MIROC-ESM_historical_r1i1p1_198101-200012.nc
katy_dir=/home/zcfakle/DATA/Downloads/
ncra -O -v va $katy_dir/va_Amon_MIROC-ESM_piControl_r1i1p1_*.nc MIROC-ESM/vaps_Aavg_MIROC-ESM_piControl_r1i1p1.nc
ncra -A -v ps $katy_dir/ps_Amon_MIROC-ESM_piControl_r1i1p1_*.nc MIROC-ESM/vaps_Aavg_MIROC-ESM_piControl_r1i1p1.nc
ncra -O -d time,'0131-01-01','0150-12-31' $katy_dir/va_Amon_MIROC-ESM_abrupt4xCO2_r1i1p1_*.nc MIROC-ESM/vaps_Aavg_MIROC-ESM_abrupt4xCO2_r1i1p1_198101-200012.nc
ncra -A -d time,'0131-01-01','0150-12-31' $katy_dir/ps_Amon_MIROC-ESM_abrupt4xCO2_r1i1p1_*.nc MIROC-ESM/vaps_Aavg_MIROC-ESM_abrupt4xCO2_r1i1p1_198101-200012.nc
ncra -O -v va $katy_dir/va_Amon_MIROC-ESM_lgm_r1i1p1_*.nc MIROC-ESM/vaps_Aavg_MIROC-ESM_lgm_r1i1p1.nc
ncra -A -v ps $katy_dir/ps_Amon_MIROC-ESM_lgm_r1i1p1_*.nc MIROC-ESM/vaps_Aavg_MIROC-ESM_lgm_r1i1p1.nc
ncra -O -v va $katy_dir/va_Amon_MIROC-ESM_midHolocene_r1i1p1_*.nc MIROC-ESM/vaps_Aavg_MIROC-ESM_midHolocene_r1i1p1.nc
ncra -A -v ps $katy_dir/ps_Amon_MIROC-ESM_midHolocene_r1i1p1_*.nc MIROC-ESM/vaps_Aavg_MIROC-ESM_midHolocene_r1i1p1.nc
ncra -O -d time,'2081-01-01','2100-12-31' -v va $katy_dir/va_Amon_MIROC-ESM_rcp85_r1i1p1_*.nc MIROC-ESM/vaps_Aavg_MIROC-ESM_rcp85_r1i1p1_208101-210012.nc
ncra -A -d time,'2081-01-01','2100-12-31' -v ps $katy_dir/ps_Amon_MIROC-ESM_rcp85_r1i1p1_*.nc MIROC-ESM/vaps_Aavg_MIROC-ESM_rcp85_r1i1p1_208101-210012.nc

#Testing for internal variability stuff....
pi_dir="/data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/piControl/mon/atmos/Amon/r1i1p1/v20121017/"
for i in {0..10000..240}
do
  start_date=$i
  end_date=$(($i+240))  
  ncra -O -d time,$start_date,$end_date $pi_dir/ps/ps_Amon_GISS-E2-R_piControl_r1i1p1_*.nc /tmp/ps.$i.nc
  ncra -O -d time,$start_date,$end_date $pi_dir/va/va_Amon_GISS-E2-R_piControl_r1i1p1_*.nc /tmp/va.$i.nc
done
ncrcat -O -v va /tmp/va.*.nc GISS-E2-R/va_A240_GISS-E2-R_piControl_r1i1p1.nc
ncrcat -O -v ps /tmp/ps.*.nc GISS-E2-R/ps_A240_GISS-E2-R_piControl_r1i1p1.nc
rm /tmp/va.*.nc
rm /tmp/ps.*.nc

pi_dir="/data/CMIP/cmip5/output1/NCAR/CCSM4/piControl/mon/atmos/Amon/r1i1p1/v20130325"
for i in {0..5980..240}
do
  start_date=$i
  end_date=$(($i+240))  
  ncra -O -d time,$start_date,$end_date $pi_dir/ps/ps_Amon_CCSM4_piControl_r1i1p1_080001-130012.nc /tmp/ps.$i.nc
  ncra -O -d time,$start_date,$end_date $pi_dir/va/va_Amon_CCSM4_piControl_r1i1p1_*.nc /tmp/va.$i.nc
done
ncrcat -O -v va /tmp/va.*.nc CCSM4/vaps_A240_CCSM4_piControl_r1i1p1.nc
ncrcat -A -v ps /tmp/ps.*.nc CCSM4/vaps_A240_CCSM4_piControl_r1i1p1.nc
rm /tmp/va.*.nc
rm /tmp/ps.*.nc

pi_dir="/data/CMIP/cmip5/output1/IPSL/IPSL-CM5A-LR/piControl/mon/atmos/Amon/r1i1p1/v20130506"
for i in {0..11520..240}
do
  #echo $i    
  start_date=$i
  end_date=$(($i+240))  
  ncra -O -d time,$start_date,$end_date $pi_dir/ps/ps_Amon_IPSL-CM5A-LR_piControl_r1i1p1_180001-279912.nc /tmp/ps.$i.nc
  ncra -O -d time,$start_date,$end_date $pi_dir/va/va_Amon_IPSL-CM5A-LR_piControl_r1i1p1_*.nc /tmp/va.$i.nc
done
ncrcat -O -v va /tmp/va.*.nc IPSL-CM5A-LR/vaps_A240_IPSL-CM5A-LR_piControl_r1i1p1.nc
ncrcat -A -v ps /tmp/ps.*.nc IPSL-CM5A-LR/vaps_A240_IPSL-CM5A-LR_piControl_r1i1p1.nc
rm /tmp/va.*.nc
rm /tmp/ps.*.nc

pi_dir="/data/CMIP/cmip5/output1/CNRM-CERFACS/CNRM-CM5/piControl/mon/atmos/Amon/r1i1p1/v20110701"
for i in {0..9840..240}
do
  echo $i  
  start_date=$i
  end_date=$(($i+240))  
  ncra -O -d time,$start_date,$end_date $pi_dir/ps/ps_Amon_CNRM-CM5_piControl_r1i1p1_*.nc /tmp/ps.$i.nc
  ncra -O -d time,$start_date,$end_date $pi_dir/va/va_Amon_CNRM-CM5_piControl_r1i1p1_*.nc /tmp/va.$i.nc
done
ncrcat -O -v va /tmp/va.*.nc CNRM-CM5/vaps_A240_CNRM-CM5_piControl_r1i1p1.nc
ncrcat -A -v ps /tmp/ps.*.nc CNRM-CM5/vaps_A240_CNRM-CM5_piControl_r1i1p1.nc
rm /tmp/va.*.nc
rm /tmp/ps.*.nc

pi_dir="/data/CMIP/cmip5/output1/MIROC/MIROC-ESM/piControl/mon/atmos/Amon/r1i1p1/v20120710"
for i in {0..7200..240}
do
  echo $i
  start_date=$i
  end_date=$(($i+240))  
  ncra -O -d time,$start_date,$end_date $pi_dir/ps/ps_Amon_MIROC-ESM_piControl_r1i1p1_*.nc /tmp/ps.$i.nc
  ncra -O -d time,$start_date,$end_date $pi_dir/va/va_Amon_MIROC-ESM_piControl_r1i1p1_*.nc /tmp/va.$i.nc
done
ncrcat -O -v va /tmp/va.*.nc MIROC-ESM/vaps_A240_MIROC-ESM_piControl_r1i1p1.nc
ncrcat -A -v ps /tmp/ps.*.nc MIROC-ESM/vaps_A240_MIROC-ESM_piControl_r1i1p1.nc
rm /tmp/va.*.nc
rm /tmp/ps.*.nc

pi_dir="/data/CMIP/cmip5/output1/CSIRO-QCCCE/CSIRO-Mk3-6-0/piControl/mon/atmos/Amon/r1i1p1/v20120607"
for i in {0..5520..240}
do
  echo $i
  start_date=$i
  end_date=$(($i+240))  
  ncra -O -d time,$start_date,$end_date $pi_dir/ps/ps_Amon_CSIRO-Mk3-6-0_piControl_r1i1p1_*.nc /tmp/ps.$i.nc
  ncra -O -d time,$start_date,$end_date $pi_dir/va/va_Amon_CSIRO-Mk3-6-0_piControl_r1i1p1_*.nc /tmp/va.$i.nc
done
ncrcat -O -v va /tmp/va.*.nc CSIRO-Mk3-6-0/vaps_A240_CSIRO-Mk3-6-0_piControl_r1i1p1.nc
ncrcat -A -v ps /tmp/ps.*.nc CSIRO-Mk3-6-0/vaps_A240_CSIRO-Mk3-6-0_piControl_r1i1p1.nc
rm /tmp/va.*.nc
rm /tmp/ps.*.nc
